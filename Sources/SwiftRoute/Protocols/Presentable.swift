//
//  File.swift
//  
//
//  Created by Pavel Ilin on 21.03.2021.
//

import Foundation
import UIKit

public protocol Presentable: class {
    
    func toPresent() -> UIViewController
    
}

extension UIViewController: Presentable {
    
    public func toPresent() -> UIViewController {
        self
    }
}

