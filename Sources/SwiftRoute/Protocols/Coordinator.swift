//
//  File.swift
//  
//
//  Created by Pavel Ilin on 21.03.2021.
//

import Foundation
import RxSwift

public protocol Coordinator: Presentable {
    
    associatedtype Result
    associatedtype Transition
    
    var result: PublishSubject<ModuleResult<Result, Transition>> {get}
    
    func present<M: Presentable>(module: M, animated: Bool, completion: (()->Void)?)
    func present<C: Coordinator>(coordinator: C, animated: Bool, completion: (()->Void)?)
    func push<M: Presentable>(module: M, animated: Bool)
    
}
