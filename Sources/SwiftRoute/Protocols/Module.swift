//
//  File.swift
//  
//
//  Created by Pavel Ilin on 21.03.2021.
//

import Foundation
import RxSwift

public protocol Module {
    
    associatedtype Result
    associatedtype Transition
    
    var result: PublishSubject<ModuleResult<Result, Transition>> {get}
    var bag: DisposeBag {get}
    var error: PublishSubject<String> {get}
    
}
