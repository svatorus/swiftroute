//
//  File.swift
//  
//
//  Created by Pavel Ilin on 21.03.2021.
//

import Foundation

public enum ModuleResult<R, T> {
    case success(R) // push next module or dismiss to source moodule
    case failure(Error) // present error and dismiss requered
    case canceled // dismiss
    case transition(T) // push some module with callback
}
