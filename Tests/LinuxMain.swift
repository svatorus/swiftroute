import XCTest

import SwiftRouteTests

var tests = [XCTestCaseEntry]()
tests += SwiftRouteTests.allTests()
XCTMain(tests)
