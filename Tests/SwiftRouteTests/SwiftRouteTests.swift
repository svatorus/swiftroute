import XCTest
@testable import SwiftRoute

final class SwiftRouteTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(SwiftRoute().text, "Hello, World!")
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
